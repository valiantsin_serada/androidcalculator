package com.epam.kronos.android.calculator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AndroidScreen {

	private WebDriver driver;

	public AndroidScreen() {
		driver = DriverManager.getDriver();
	}

	public void performAddition(int firstItem, int secondItem) {
		driver.findElement(By.name(Integer.toString(firstItem))).click();
		driver.findElement(By.name("+")).click();
		driver.findElement(By.name(Integer.toString(secondItem))).click();
		driver.findElement(By.name("=")).click();
	}

	public int getResult() {
		String result = driver.findElement(By.className("android.widget.EditText")).getText();
		return Integer.parseInt(result);
	}
}
