package com.epam.kronos.android.calculator;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AndroidCalcTest {
	private int firstItem;
	private int secondItem;
	private AndroidScreen androidScreen = new AndroidScreen();

	@BeforeTest
	public void setUp() {
		firstItem = 2;
		secondItem = 3;
	}

	@Test
	public void Sum() {
		androidScreen.performAddition(firstItem, secondItem);
		Assert.assertEquals(Math.addExact(firstItem, secondItem), androidScreen.getResult());
	}

	@AfterTest
	public void quitAction() {
		DriverManager.getDriver().quit();
	}
}
